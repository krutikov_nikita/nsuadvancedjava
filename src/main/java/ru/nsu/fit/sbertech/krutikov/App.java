package ru.nsu.fit.sbertech.krutikov;

/**
 * Created by nikitoss on 05.04.16.
 */

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.*;

public class App {
    private static final Logger LOG = LogManager.getLogger(App.class);
    private SortedMap<String, Long> userEdits = new TreeMap<>();
    private SortedMap<Long, Long> nodeKeys = new TreeMap<>();

    private static <K,V extends Comparable<? super V>>
    SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
        SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
                new Comparator<Map.Entry<K,V>>() {
                    @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
                        int res = e2.getValue().compareTo(e1.getValue());
                        return res != 0 ? res : 1;
                    }
                }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

    public static void main(String[] args) throws ArchiveException {
        ///Option option = new Option("l", "login", true, "Login");
        String filePath = "./resources/RU-NVS.osm.bz2";

        App app = new App();
        LOG.info("Application started");
        try (CompressorInputStream compressorInputStream = decompressFile(filePath)) {
            LOG.info("CompressorInputStream was get");
            app.readXML(compressorInputStream);
        } catch (CompressorException e) {
            LOG.error("Problems in compression file" + filePath, e);
        } catch (FileNotFoundException e) {
            LOG.error("File" + filePath + "did't found", e);
        } catch (IOException e) {
            LOG.error("Can't close stream", e);
        }

        app.userEdits.values();

        int i=0;
        for (Map.Entry<String, Long> entry  : entriesSortedByValues( app.userEdits )) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
            if (++i>10) break;
        }
        System.out.println("==========================");
        i=0;
        for (Map.Entry<Long, Long> entry  :  entriesSortedByValues(app.nodeKeys)) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
            if (++i>10) break;
        }

        LOG.info("Application finished");

    }

    public static CompressorInputStream decompressFile(String filePath) throws FileNotFoundException, CompressorException {
        return new CompressorStreamFactory()
                .createCompressorInputStream(new BufferedInputStream(new FileInputStream(filePath)));
    }

    public void statistics(Long id, String user) {
        userEdits.compute(user, (k, v) -> (v == null) ? 1 : v + 1);
        nodeKeys.compute(id, (k, v) -> (v == null) ? 1 : v + 1);
    }

    public void readXML(InputStream input) {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader r = null;

        try {
            r = factory.createXMLStreamReader(input);

            int event = r.getEventType();
            while (true) {
                switch (event) {
                    case XMLStreamConstants.START_DOCUMENT:
                        LOG.info("Start Document.");
                        break;
                    case XMLStreamConstants.START_ELEMENT:
                        LOG.info("Start Element: " + r.getName() + " arguments:" + r.getAttributeCount());
                        if (r.getName().toString().equals("node")) {
                            Long id = (long) -1;
                            String user = "unknown";
                            for (int i = 0, n = r.getAttributeCount(); i < n; ++i) {
                                String attributeName = r.getAttributeName(i).toString();
                                if (attributeName.equals("id"))
                                    id = Long.parseLong(r.getAttributeValue(i));
                                if (attributeName.equals("user"))
                                    user = r.getAttributeValue(i);
                            }
                            this.statistics(id, user);
                        }
                        break;
                    case XMLStreamConstants.END_DOCUMENT:
                        LOG.info("End Document.");
                        break;
                }

                if (!r.hasNext())
                    break;

                event = r.next();
            }

        } catch (XMLStreamException e) {
            LOG.error("Problems with XML", e);
        } finally {
            if (r != null) try {
                r.close();
            } catch (XMLStreamException e) {
                LOG.error("Can't close XMLStreamReader");
            }
        }
    }
}
